#ifndef __w1_PORT
//#include "onewiredef.h"
#endif

#include "AOneWire.h"
#include "crc.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>

#if !defined __w1_PORT || !defined __w1_DDR || !defined __w1_PIN ||            \
    !defined __w1_bit
#define __w1_PORT PORTB // Definyce pripojeni zbernice
#define __w1_DDR DDRB
#define __w1_PIN PINB
#define __w1_bit PB2
#endif

#if !defined __W1_BUS_GET || !defined __W1_BUS_CLR || !defined __W1_BUS_SET
#define __W1_BUS_GET (__w1_PIN & (1 << __w1_bit)) // Precte stav zbernice
#define __W1_BUS_CLR                                                           \
  {                                                                            \
    __w1_PORT &= ~(1 << __w1_bit);                                             \
    __w1_DDR |= (1 << __w1_bit);                                               \
  } // Vysle na sbernici 0.
#define __W1_BUS_SET                                                           \
  {                                                                            \
    __w1_PORT &= ~(1 << __w1_bit);                                             \
    __w1_DDR &= ~(1 << __w1_bit);                                              \
  } // Nastavi zbernici na 1 / pro prijem.
#define __W1_BUS_POWER                                                         \
  {                                                                            \
    __w1_PORT |= (1 << __w1_bit);                                              \
    __w1_DDR |= (1 << __w1_bit);                                               \
  } // Nastavi zbernici pro parazitni napajeni.
#endif

#if defined __w1_1_PORT &&                                                     \
    (!defined __W1_1_BUS_GET || !defined __W1_1_BUS_CLR ||                     \
     !defined __W1_1_BUS_SET)
#define __W1_1_BUS_GET (__w1_1_PIN & (1 << __w1_1_bit)) // Precte stav zbernice
#define __W1_1_BUS_CLR                                                         \
  {                                                                            \
    __w1_1_PORT &= ~(1 << __w1_1_bit);                                         \
    __w1_1_DDR |= (1 << __w1_1_bit);                                           \
  } // Vysle na sbernici 0.
#define __W1_1_BUS_SET                                                         \
  {                                                                            \
    __w1_1_PORT &= ~(1 << __w1_1_bit);                                         \
    __w1_1_DDR &= ~(1 << __w1_1_bit);                                          \
  } // Nastavi zbernici na 1 / pro prijem.
#define __W1_1_BUS_POWER                                                       \
  {                                                                            \
    __w1_1_PORT |= (1 << __w1_1_bit);                                          \
    __w1_1_DDR |= (1 << __w1_1_bit);                                           \
  } // Nastavi zbernici pro parazitni napajeni.
#endif

/**
 * Provede inicializaci/reset sbernice. Vrati 1 pokud na zbernici nenastane
 * odezva - chyba.
 */
uint8_t _w1_reset(uint8_t nn = 0) {
  uint8_t tSREG;
#if defined __w1_1_PORT
  switch (nn) {
  case 0:
#endif
    tSREG = SREG;
    cli();
    __W1_BUS_CLR
    _delay_us(480);
    __W1_BUS_SET
    _delay_us(12);
    if (__W1_BUS_GET) {
      _delay_us(77);
      if (!(__W1_BUS_GET)) {
        SREG = tSREG;
        _delay_us(400);
        return (0);
      };
    };
    SREG = tSREG;
#if defined __w1_1_PORT
    break;
  case 1:
    tSREG = SREG;
    cli();
    __W1_1_BUS_CLR
    _delay_us(480);
    __W1_1_BUS_SET
    _delay_us(12);
    if (__W1_1_BUS_GET) {
      _delay_us(77);
      if (!(__W1_1_BUS_GET)) {
        SREG = tSREG;
        _delay_us(400);
        return (0);
      };
    };
    SREG = tSREG;
    break;
  }
#endif
  return (1);
}

/**
 * Precte/zapise jeden bit z/na sbernici (cteni se provadi s d.0=1 :) )
 *
 * @param d Zapisovana data - vyznam ma pouze nejnizsi bit - bit 0.
 * @return  precteny bit
 */
uint8_t _w1_rw_bit(uint8_t d, uint8_t nn = 0) {
  uint8_t tSREG;
#if defined __w1_1_PORT
  switch (nn) {
  case 0:
#endif
    tSREG = SREG;
    cli();
    __W1_BUS_CLR
    _delay_us(1);
    // d >>= 1;         //Moc velka optimalizace. Pokud je pouze jedna sbernice,
    // asm("BRCC AAA"); //funguje to OK. Pri vice sbernicich jiz nee.
    // __W1_BUS_SET     //Zrejme je problem, ze s jednou sbernici je d
    // asm("AAA:");     //v registru, pri vice sbernicich jiz nee.
    //                    Optimalizator to pak zoptimalizuje do nefunkcna.
    //                    Ciste C reseni je sice vetsi, ale mene rozbitne.
    if (d & 0x01) {
      __W1_BUS_SET
    }
    d >>= 1;
    _delay_us(10);
    d &= 0x7f;
    if (__W1_BUS_GET)
      d |= 0x80;
    _delay_us(75);
    __W1_BUS_POWER
    _delay_us(5);
    SREG = tSREG;
#if defined __w1_1_PORT
    break;
  case 1:
    tSREG = SREG;
    cli();
    __W1_1_BUS_CLR
    _delay_us(1);
    if (d & 0x01) {
      __W1_1_BUS_SET
    }
    d >>= 1;
    _delay_us(10);
    d &= 0x7f;
    if (__W1_1_BUS_GET)
      d |= 0x80;
    _delay_us(75);
    __W1_1_BUS_POWER
    _delay_us(5);
    SREG = tSREG;
    break;
  }
#endif

  return (d);
}

/**
 * Zapise/precte jeden byte na sbernici
 * cte se s data = 0xff
 */
uint8_t _w1_write(uint8_t data, uint8_t nn = 0) {
  for (uint8_t i = 8; i; i--) {
    data = _w1_rw_bit(data, nn);
  };
  return (data);
}

uint8_t w1_search_next(uint8_t diff, uint8_t romcode[8], uint8_t nn) {
  uint8_t i, b, next_diff;
  next_diff = 0x00;
  if (diff != 0) {
    if (_w1_reset(nn))
      return 0xff;
    _w1_write(0xf0, nn);
    i = 8 * 8;
    do {
      do {
        b = _w1_rw_bit(1, nn); // read bit
        if (b)
          b = 0x01;
        if (_w1_rw_bit(1, nn)) { // read complement bit
          if (b)                 // 11
            return 0xff;         // data error
        } else {
          if (!b) { // 00 = 2 devices
            if (diff > i || ((*romcode & 1) && diff != i)) {
              b = 1;         // now 1
              next_diff = i; // next pass 0
            }
          }
        }
        _w1_rw_bit(b, nn); // write bit
        *romcode >>= 1;
        if (b)
          *romcode |= 0x80; // store bit
        i--;
      } while (i & 0x07);
      romcode++;
    } while (i);
  };
  return next_diff;
}

uint8_t w1_search(uint8_t maxdevices, uint8_t *romcode, uint8_t nn) {
  uint8_t devices = 0, diff = 0xff, i;

  for (;;) {
    diff = w1_search_next(diff, romcode, nn);
    if (diff == 0xff) {
      return (0);
    };
    devices++;
    if (devices >= maxdevices)
      return (devices);
    for (i = 0; i < 8; i++)
      romcode[i + 8] = romcode[i];
    if (diff == 0) {
      return (devices);
    };
    romcode += 8;
  };
}

/**
 * Provede naadresovani zarizenni. Pokud je romcode == NULL a nebo prvni byte
 * odresy 0, aktivuji se vsevhna zarizeni na sbernici. Vrati 1, pokud je
 * detekovano zadne zarizeni.
 */
uint8_t _w1_match_rom(uint8_t romcode[8], uint8_t nn = 0) {
  uint8_t i;
  if (_w1_reset(nn)) {
    return (1);
  }
  if ((romcode == NULL) || (romcode[0] != 0x28)) {
    _w1_write(0xcc, nn);
  } else {
    _w1_write(0x55, nn);
    for (i = 8; i; i--) {
      _w1_write(*romcode, nn);
      romcode++;
    };
  };
  return (0);
}

uint8_t w1_ds18B20_setresolution(uint8_t romcode[8], uint8_t resolution,
                                 uint8_t nn) {
  if (_w1_match_rom(romcode, nn)) {
    return (1);
  }
  _w1_write(0x4E, nn);
  _w1_write(0x00, nn);
  _w1_write(0x00, nn);
  _w1_write(resolution, nn);
  return (0);
}

uint8_t w1_ds18B20_convert(uint8_t romcode[8], uint8_t nn) {
  if (_w1_match_rom(romcode, nn)) {
    return (1);
  }
  _w1_write(0x44, nn);
  return (0);
}

uint8_t w1_ds18B20_get_temperature(uint8_t romcode[8], int16_t *temp,
                                   uint8_t nn) {
  uint8_t crc = 0;

  if (_w1_match_rom(romcode, nn)) {
    return (1);
  }
  _w1_write(0xBE, nn);

  for (uint8_t i = 0; i < 8; i++) {
    register uint8_t a;
    a = _w1_write(0xff, nn);
    crc = crc8(crc, a);
    if (i <= 1)
      ((uint8_t *)temp)[i] = a;
  };
  if (crc != _w1_write(0xff, nn)) {
    return (3);
  }
  return (0);
}

uint8_t w1_ds18B20_temperature(uint8_t romcode[8], int16_t *temp, uint8_t nn) {
  if (w1_ds18B20_convert(romcode, nn)) {
    return (1);
  }
  _delay_ms(800);
  return (w1_ds18B20_get_temperature(romcode, temp, nn));
}

int16_t temperature16to10(int16_t temp) { return ((temp * 5) >> 3); }

float temperature16tofloat(int16_t temp) { return temp / 16.0; }
