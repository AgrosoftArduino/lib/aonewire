#ifndef _DS18B20_H
#define _DS18B20_H

#include <stdio.h>

/**
 * Provede vyhledani zarizeni na sbernici.
 * @param maxdevices maximalni pocet moznych zarizeni, pokud je zarizeni vice,
                     v romcode bude uvedeno pouze prvnich maxdevices zarizeni.
 * @param romcode adresa na blok pameti o velikosti maxdevices*8 pro ulozeni
      adres nalezenych zarizeni.
 * @param nn Cislo w1 sbernice.
 * @return  Vrati pocet skutecne nalezenych zarizeni.
 */
uint8_t w1_search(uint8_t maxdevices, uint8_t *romcode, uint8_t nn = 0);

/**
 * Provede vyhledani dalsiho zarizeni na sbernici.
 * @param diff - cislo urcujici cislo bitu v adrese ...........
 * @param romcode - adresa nalezeneho zarizeni
 * @param nn Cislo w1 sbernice.
 * @return  vrati novou hodnotu diff, pokud vrati 0xff, uz se nic nenajde,
 * hledani je potreba ukoncit.
 */
uint8_t w1_search_next(uint8_t diff, uint8_t romcode[8], uint8_t nn = 0);

/**
 * Provede konverzi teploty na danem teplomeru.
 * Pokud je romcode==NULL, provede prevod kazdy teplomer.
 * @param romcode
 * @param nn Cislo w1 sbernice.
 * @return Vrati 1, pokud je detekovano zadne zarizeni.
 */
uint8_t w1_ds18B20_convert(uint8_t romcode[8], uint8_t nn = 0);

/**
 * Nastavi rozliseni teplomeru (9 | 10 | 11 | 12bit).
 * Pokud je romcode==NULL, provede prevod kazdy teplomer.
 * @param romcode
 * @param nn Cislo w1 sbernice.
 * @param resolution
 * @return  Vrati 1, pokud je detekovano zadne zarizeni.
 */
uint8_t w1_ds18B20_setresolution(uint8_t romcode[8], uint8_t resolution,
                                 uint8_t nn = 0);

/**
 * Nacte teplotu z daneho teplomeru.
 * Prepise romcode prijatymi daty
 * @param romcode Pokud je na sbernici pouze jedno zarizeni, lze pouzit
 * romcode==NULL
 * @param temp Nactena teplota je v 16tinach stupne Celsia. (Jako je ulozeno
 * primo v cidle)
 * @param nn Cislo w1 sbernice.
 * @return Vrati 0 - vse v poradku
 *             <>0 - nastala chyba
 */
uint8_t w1_ds18B20_get_temperature(uint8_t romcode[8], int16_t *temp,
                                   uint8_t nn = 0);

/**
 * Provede konverzi a nacteni teploty z teplomeru.
 * @param romcode Pokud je na sbernici pouze jedno zarizeni, lze pouzit
 * romcode==NULL
 * @param temp Nactena teplota je v 16tinach stupne Celsia. (Jako je ulozeno
 * primo v cidle)
 * @param nn Cislo w1 sbernice.
 * @return Vrati 0 - vse v poradku
 *          <>0 - nastala chyba
 */
uint8_t w1_ds18B20_temperature(uint8_t romcode[8], int16_t *temp,
                               uint8_t nn = 0);

/**
 * Prevede teplotu reprezentovanou v 16inach stupne na 10iny stupne
 * @param temp teplota v 16inach stupne
 * @return teplota v 10inach stupne.
 */
int16_t temperature16to10(int16_t temp);

/**
 * Prevede teplotu reprezentovanou v 16inach stupne na float
 * @param temp teplota v 16inach stupne
 * @return teplota.
 */
float temperature16tofloat(int16_t temp);

#endif
