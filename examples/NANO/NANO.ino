#include "AOneWire.h"

// DS18B20 pripojit na DS18B20 na D10
// Mezi D10 a +5V pripojit odpor 4.7k
// Mozne pripojit az 8 teplomeru. Dalsi jsou ignorovany.

uint8_t ndevices;

uint8_t romcode[8][8];

void setup() {
  Serial.begin(9600);
  Serial.println("AOneWire demo");
  ndevices = w1_search(8, (uint8_t *)&romcode);
}

uint16_t i = 0;

void loop() {
  i++;
  Serial.print(i);
  Serial.print(": ");
  w1_ds18B20_convert(NULL);

  delay(1000);
  for (uint8_t j = 0; j < ndevices; j++) {
    int16_t temperature16;
    if (w1_ds18B20_get_temperature(romcode[j], &temperature16) == 0) {
      Serial.print(temperature16tofloat(temperature16));
    } else {
      Serial.print(F("Error!"));
    }
    Serial.print(';');
  }

  Serial.println();
}
